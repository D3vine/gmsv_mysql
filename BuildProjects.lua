solution "MySQL Module"

	language "C++"
	location ( "solutions/" .. os.get() .. "-" .. _ACTION )
	flags { "Symbols", "NoEditAndContinue", "NoPCH", "StaticRuntime", "EnableSSE" }
	includedirs {	"include/gmodlua/include/",
					"include/mysql/" .. ((os.get() == "linux" and "linux/") or "win32/") .. "include/",
					"projects/gmsv_mysql/gmsv_MySQL/includes/",
					"projects/gmsv_mysql/lib_ThreadOO/includes/",
					"projects/gmsv_mysql/lib_LuaOO/includes/"		 }

	if os.get() == "linux" then
		buildoptions{ "-fPIC -m32 -std=c++11" }
		linkoptions{ "-fPIC ../../include/mysql/linux/lib/libmysqlclient.a" }
	end

	configurations
	{
		"Release"
	}

	configuration "Release"
		platforms { "x32" }
		defines { "NDEBUG" }
		flags{ "Optimize", "FloatFast" }

		if os.get() == "windows" then
			defines{ "WIN32" }
		elseif os.get() == "linux" then
			defines{ "LINUX" }
		end

	project "lib_ThreadOO"
		defines { "GMMODULE" }
		files { "projects/gmsv_mysql/lib_ThreadOO/source/**.*", "projects/gmsv_mysql/lib_ThreadOO/includes/**.*" }
		kind "StaticLib"
		targetdir ( "lib/" .. os.get() .. "/" )

	project "lib_LuaOO"
		defines{ "GMMODULE" }
		files { "projects/gmsv_mysql/lib_LuaOO/source/**.*", "projects/gmsv_mysql/lib_LuaOO/includes/**.*" }
		kind "StaticLib"
		targetdir ( "lib/" .. os.get() .. "/" )

	project "gmsv_MySQL"
		defines{ "GMMODULE" }
		files{ "projects/gmsv_mysql/gmsv_MySQL/source/**.*", "projects/gmsv_mysql/gmsv_MySQL/includes/**.*" }
		kind "SharedLib"
		targetdir ( "release/" .. os.get() .. "/" )
		local libmysql
		if (os.get() == "linux") then
			libmysql = nil
			libdirs {"include/mysql/linux/lib/", "lib/linux/"}
		else
			libmysql = "mysqlclient.lib"
			libdirs {"include/mysql/win32/lib/vs12/", "lib/windows/"}
		end
		links { libmysql, "lib_ThreadOO", "lib_LuaOO" }
		local underscore = (os.get() == "linux" and "linux" or "win32")
		targetname("gmsv_mysql_" .. underscore)
