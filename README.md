# **gmsv_mysql**
##### gmsv_mysql is a module for Garry's Mod servers that enables communication with a MySQL server. The other widely available MySQL modules are gmsv_tmysql and gmsv_mysqloo. Both modules do their job but lack features that the other has. Thus, gmsv_mysql aims to provide the best of both worlds.

## Features
* **Fully object-oriented design**
* **Support for multiple resultsets**
* **Executes queries in the order they're created**
* **Fully asynchronous (except if you want instant results!)**
* **Queries can hold the main thread on request**
* **Prepared Statements!**

## Building
Linux and Windows builds should be straightforward. Premake5 is required, and BuildProjects.bat will plop solution files into a solutions folder. Under Windows, to build the module with the libmysql.dll requirement, link against libmysql instead of libmysqlclient.

## Installation
Place the built DLL into garrysmod/lua/bin. On Linux, place libmysqlclient.so from the packaged C Connector API into the folder with srcds. The Windows build has libmysqlclient linked into the DLL so that this isn't required.

## Disclaimers
The MySQL C Connector APIs for both Windows and Linux are included in this repository. This is only a convenience to those wanting to build the module. We don't claim any credit for it and if Oracle wishes, we will remove it from the repository. Additionally, when updates to the module are made, we will ensure that the packaged connector API is up-to-date.

Prepared Statements functionality is emulated. True prepared statements using the C Connector API typically expects the compiler to know beforehand how much data will go to and be received from the MySQL server. Since that simply won't be the case, our prepared statements work as you would expect one to, though they're fundamentally different. The module escapes the input and formulates a finished query which runs normally. This means that typical benefits won't be observed (except faster escaping and convenience). It is possible to implement true prepared statements, but it involves a fair amount of memory management. Maybe we'll have them in the future.

Much code in this module will be reminiscent of gmsv_mysqloo and gmsv_tmysql. The goal with gmsv_mysql is only to bring every feature possible to one module.

# Documentation
## Global
```
DATABASE_CONNECTED = 0
DATABASE_CONNECTING = 1
DATABASE_NOT_CONNECTED = 2
DATABASE_INTERNAL_ERROR = 3

QUERY_NOT_RUNNING = 0
QUERY_RUNNING = 1
QUERY_READING_DATA = 2
QUERY_COMPLETE = 3
QUERY_ABORTED = 4
QUERY_WAITING = 5

CLIENT_INTERACTIVE = 1024
CLIENT_MULTI_STATEMENTS = 65536
CLIENT_MULTI_RESULTS = 131072
```
## Module Table
```
mysql.MODULE_VERSION -- Returns an integer that represents the module version
mysql.MODULE_INFO -- Returns a string that represents the module version
mysql.MYSQL_VERSION -- Returns an integer that represents the MySQL client library version
mysql.MYSQL_INFO -- Returns a string that represents the MySQL client library version

mysql.Connect(<string host>, <string user>, <string pass>, <string database>, [number port = 3306], [string unixSocket = 0], [number clientFlags = interactive,multistatements,multiresults]) -- Returns a database object configured with the given parameters
```
## Database Object
```
dbase:Connect() -- Initiates a connection to the MySQL server
dbase:Wait()
dbase:Disconnect() -- Closes the connection to the MySQL server
dbase:Query(<string query>) -- Returns a query object configured with the given query
dbase:Prepare(<string query>) -- Returns a statement object to be used as a prepared statement
dbase:Status() -- Returns the current connection status to the MySQL server
dbase:Escape(<string>) -- Returns the escaped version of the input
dbase:GetCharSet() -- Returns the current character set
dbase:SetCharSet(<string charSet>) -- Sets the current character set
dbase:HostInfo() -- Returns the current host info (e.g. 'thed3vine.net via TCP/IP')
dbase:ServerVersion() -- Returns an integer that represents the MySQL server's version
dbase:ServerInfo() -- Returns a string that represents the MySQL server's version
dbase:GetAllQueries() -- Returns a table of all processing and waiting query objects

function dbase.OnConnected(self) -- Called by the module when the database successfully connects
function dbase.OnConnectionFailed(self, err) -- Called by the module when the database connection fails
```
## Query Object
```
query:Start() -- Adds the query to the end of the database object's processing stack
query:Wait() -- Processes the query while holding the main thread
query:Abort() -- Cancels the query if it isn't already processing
query:IsRunning() -- Returns true if the query is being processed
query:Status() -- Returns the current status of the query
query:GetData() -- If the query has completed, returns a reference to the table passed to query.OnCompleted

function query.OnCompleted(self, results) -- Called by the module when the query is completed
```
## Statement Object
##### Prepared statements are a convenient way to run the same query multiple times with different inputs. Supply a query to dbase:Prepare() using question marks to symbolize the input you'll give to it later, then call statement:Run() with the variables you'd like to use. The module will take care of plopping your input into the full query and running it, returning to a callback. statement:Run will take any amount of arguments (including 0) and the callback is optional.
```
statement:Run([arg1], [arg2], [arg3], [function callBack(table results)])
statement:Wait(<bool shouldWait = false>) -- If true, future statement:Run calls will process synchronously
```
## Results Structure
##### Overwrite your query object's OnCompleted function to run when it's finished.
```text
{
  1 = { -- First resultset
    Success = true,
    Affected = 1, -- Number of affected rows. Represents number of received rows in select queries
    LastID = 0, -- Number representing the last inserted ID. 0 if the table does not auto_increment
    Data = {
      -- Table containing all received rows. Each row uses named fields
    }
  },

  2 = { -- Second resultset
    Success = false,
    ErrorID = 1146,
    Error = "Table 'database.table' doesn't exist"
  },

  3 = { -- Third resultset
    Success = false,
    Aborted = true
  }
}
```
