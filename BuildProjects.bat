@ECHO OFF

echo Creating Windows solutions
premake5 --os=windows --file=BuildProjects.lua vs2013

echo.
echo.
echo Creating Linux solutions
premake5 --os=linux --file=BuildProjects.lua gmake

echo.
echo.
echo Solutions created inside the solutions folder
pause