#include "GarrysMod/Lua/Interface.h"
#include "CLASS_Database.h"
#include "CLASS_Query.h"

#define MODULE_VERSION "1.0"
#define MODULE_VERSION_NUMERIC 10

std::vector<Database*> DATABASES;

int DatabaseConnect(lua_State* state)
{
	/* Arguments:
	1 = host
	2 = username
	3 = password
	4 = database
	5 = port
	6 = unixSocket
	7 = clientFlag
	*/

	Database* dbase = new Database(state);

	int nNumParameters = LUA->Top();
	if (nNumParameters > 0)
	{
		if (!LuaOO::checkArgument(state, 1, GarrysMod::Lua::Type::STRING))
			return 0;
		dbase->setHost(LUA->GetString(1));
	}
	if (nNumParameters > 1)
	{
		if (!LuaOO::checkArgument(state, 2, GarrysMod::Lua::Type::STRING))
			return 0;
		dbase->setUser(LUA->GetString(2));
	}
	if (nNumParameters > 2)
	{
		if (!LuaOO::checkArgument(state, 3, GarrysMod::Lua::Type::STRING))
			return 0;
		dbase->setPass(LUA->GetString(3));
	}
	if (nNumParameters > 3)
	{
		if (!LuaOO::checkArgument(state, 4, GarrysMod::Lua::Type::STRING))
			return 0;
		dbase->setDatabase(LUA->GetString(4));
	}
	if (nNumParameters > 4)
	{
		if (!LuaOO::checkArgument(state, 5, GarrysMod::Lua::Type::NUMBER))
			return 0;
		dbase->setPort((unsigned int)LUA->GetNumber(5));
	}
	if (nNumParameters > 5)
	{
		if (!LuaOO::checkArgument(state, 6, GarrysMod::Lua::Type::STRING))
			return 0;
		dbase->setUnixSocket(LUA->GetString(6));
	}
	if (nNumParameters > 6)
	{
		if (!LuaOO::checkArgument(state, 7, GarrysMod::Lua::Type::NUMBER))
			return 0;
		dbase->setClientFlag((unsigned int)LUA->GetNumber(7));
	}

	dbase->pushObject();

	DATABASES.push_back(dbase);
	return 1;
}

GMOD_MODULE_OPEN()
{
	if (mysql_library_init(0,NULL,NULL))
	{
		LUA->ThrowError("Error: Couldn't initialize libmysql!");
		return 0;
	}

	LuaOO::instance()->registerPollingFunction(state, "MySQL::Poll");
	LuaOO::instance()->registerClasses(state);

	LUA->PushSpecial(GarrysMod::Lua::SPECIAL_GLOB);
		LUA->CreateTable();
			LUA->PushNumber(MODULE_VERSION_NUMERIC); LUA->SetField(-2, "MODULE_VERSION");
			LUA->PushString(MODULE_VERSION); LUA->SetField(-2, "MODULE_INFO");
			LUA->PushNumber((float)mysql_get_client_version()); LUA->SetField(-2, "MYSQL_VERSION");
			LUA->PushString(mysql_get_client_info()); LUA->SetField(-2, "MYSQL_INFO");

			LUA->PushCFunction(DatabaseConnect); LUA->SetField(-2, "Connect");
		LUA->SetField(-2, "mysql");

		LUA->PushNumber((float)DATABASE_CONNECTED); LUA->SetField(-2, "DATABASE_CONNECTED");
		LUA->PushNumber((float)DATABASE_CONNECTING); LUA->SetField(-2, "DATABASE_CONNECTING");
		LUA->PushNumber((float)DATABASE_NOT_CONNECTED); LUA->SetField(-2, "DATABASE_NOT_CONNECTED");
		LUA->PushNumber((float)DATABASE_INTERNAL_ERROR); LUA->SetField(-2, "DATABASE_INTERNAL_ERROR");

		LUA->PushNumber((float)QUERY_NOT_RUNNING); LUA->SetField(-2, "QUERY_NOT_RUNNING");
		LUA->PushNumber((float)QUERY_RUNNING); LUA->SetField(-2, "QUERY_RUNNING");
		LUA->PushNumber((float)QUERY_READING_DATA); LUA->SetField(-2, "QUERY_READING_DATA");
		LUA->PushNumber((float)QUERY_COMPLETE); LUA->SetField(-2, "QUERY_COMPLETE");
		LUA->PushNumber((float)QUERY_ABORTED); LUA->SetField(-2, "QUERY_ABORTED");
		LUA->PushNumber((float)QUERY_WAITING); LUA->SetField(-2, "QUERY_WAITING");

		LUA->PushNumber((float)CLIENT_MULTI_RESULTS); LUA->SetField(-2, "CLIENT_MULTI_RESULTS");
		LUA->PushNumber((float)CLIENT_MULTI_STATEMENTS); LUA->SetField(-2, "CLIENT_MULTI_STATEMENTS");
		LUA->PushNumber((float)CLIENT_INTERACTIVE); LUA->SetField(-2, "CLIENT_INTERACTIVE");
	LUA->Pop();

	return 0;
}

GMOD_MODULE_CLOSE()
{
	LuaOO::shutdown();

	mysql_library_end();
	return 0;
}
