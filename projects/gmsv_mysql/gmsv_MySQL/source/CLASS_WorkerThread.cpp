#include "CLASS_MutexLocker.h"
#include "CLASS_WorkerThread.h"
#include "CLASS_Query.h"
#include "CLASS_Database.h"

WorkerThread::WorkerThread(Database* dbase)
	: Thread(), m_dbase(dbase)
{

}

WorkerThread::~WorkerThread(void)
{
}

void WorkerThread::reset()
{
	m_sql = 0;
	m_finished = false;
}

bool WorkerThread::init()
{
	reset();

	mysql_thread_init();

	return true;
}

int WorkerThread::run()
{
	if (m_query->getAborted()) {
		m_query->triggerCallback(QUERY_ABORTED);
		return 0;
	}

	m_sql = m_dbase->lockHandle();

	if (mysql_query(m_sql, m_query->getQuery().c_str()) != 0)
	{
		m_query->triggerCallback(QUERY_ERROR);
		return 0;
	}

	m_query->triggerCallback(QUERY_SUCCESS);

	return 0;
}

void WorkerThread::exit()
{
	m_query->setWorker(NULL);

	if (m_sql)
	{
		m_dbase->unlockHandle();
		mysql_thread_end();
	}

	setQuery(NULL);
	setFinished(true);
}