#include "CLASS_Database.h"
#include "CLASS_Statement.h"
#include "CLASS_Query.h"
#include "CLASS_Result.h"

#ifdef LINUX
#include <stdlib.h>
#include <string.h>
#endif

Statement::Statement(Database* dbase, lua_State* state)
	: LuaObjectBaseTemplate<Statement>(state), m_dbase(dbase)
{
}

Statement::~Statement()
{
}

BEGIN_BINDING(Statement);
BIND_FUNCTION("Run", Statement::run);
BIND_FUNCTION("Wait", Statement::wait);
END_BINDING();

void Statement::poll()
{
}

int Statement::run()
{
	int numParameters = MLUA->Top();
	if (numParameters > 1)
	{
		std::string manipulate = std::string(m_query);
		std::size_t pos = 0;
		int curParameter = 2;
		int cbRef = NULL;

		if (numParameters >= curParameter)
		{
			while (true)
			{
				if (MLUA->GetType(curParameter) == GarrysMod::Lua::Type::FUNCTION)
				{
					MLUA->Push(curParameter);
					cbRef = MLUA->ReferenceCreate();

					curParameter++;
					continue;
				}

				pos = manipulate.find("?", pos);

				if (pos == std::string::npos)
					break;

				unsigned int length;
				const char* input = MLUA->GetString(curParameter, &length);
				char* var = m_dbase->Escape(input, length);
				curParameter++;

				manipulate.replace(pos, 1, var);

				if (curParameter > numParameters)
					break;

				free(var);

				int toAdd = strlen(var) - 1;
				pos = pos + toAdd;
			}
		}
		Query* query = new Query(m_dbase, m_luaState);
		query->setPrepared(true);

		query->setQuery(manipulate);

		if (IS_NOT_NULL(cbRef))
		{
			query->setCallback(cbRef);
		}

		if (m_wait)
			query->wait();
		else
			query->start();
	}
	else
	{
		Query* query = new Query(m_dbase, m_luaState);
		query->setQuery(m_query);
		query->setPrepared(true);

		if (m_wait)
			query->wait();
		else
			query->start();
	}

	return 0;
}

int Statement::wait()
{
	if (!checkArgument(2, GarrysMod::Lua::Type::BOOL))
		return 0;

	m_wait = MLUA->GetBool(2);

	return 0;
}
