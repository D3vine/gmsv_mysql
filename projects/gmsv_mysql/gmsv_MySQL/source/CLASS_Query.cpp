#include "CLASS_Query.h"
#include "CLASS_Database.h"
#include "CLASS_WorkerThread.h"
#include "CLASS_Result.h"

#ifdef LINUX
#include <stdlib.h>
#endif

Query::Query(Database* dbase, lua_State* state)
	: LuaObjectBaseTemplate<Query>(state), m_database(dbase)
{
	m_status = QUERY_NOT_RUNNING;
}

Query::~Query(void)
{
	if (m_status == QUERY_WAITING || (running() && !m_waited))
		wait();

	m_database->clearFromStack(this);
}

bool Query::canDelete()
{
	if (getStatus() != QUERY_DELETE_READY)
		return false;

	return LuaObjectBaseTemplate<Query>::canDelete();
}

BEGIN_BINDING(Query);
BIND_FUNCTION("Start", Query::start);
BIND_FUNCTION("IsRunning", Query::isRunning);
BIND_FUNCTION("Abort", Query::abort);
BIND_FUNCTION("Status", Query::status);
BIND_FUNCTION("Wait", Query::wait);
END_BINDING();

void Query::poll()
{
	if (m_shouldCallback)
	{
		callback();

		setStatus(QUERY_DELETE_READY);

		if (m_prepared)
		{
			this->luaUnRef();
			delete this;
		}
	}
}

bool Query::running()
{
	if (m_worker || m_waited)
		return true;

	return false;
}

int Query::start()
{
	if (!m_prepared)
	{
		pushObject();
		MLUA->GetField(-1, "OnCompleted");
		setCallback(MLUA->ReferenceCreate());
		MLUA->Pop();
	}

	m_database->addQueryToStack(this);
	m_status = QUERY_WAITING;

	return 0;
}

int Query::isRunning()
{
	MLUA->PushBool(running());

	return 1;
}

int Query::abort()
{
	// If a query's already been started, we can't abort it :(
	if (!running())
	{
		setAborted(true);
	}

	MLUA->PushBool(!running());

	return 1;
}

int Query::status()
{
	MLUA->PushNumber(m_status);

	return 1;
}

int Query::wait()
{
	// If the query already has a thread working on it, wait that thread
	// else database needs to create a thread for this query and wait it
	if (getWorker())
		getWorker()->wait();
	else
	{
		setStatus(QUERY_RUNNING);
		m_waited = true;

		if (getAborted())
		{
			triggerCallback(WorkerThread::QUERY_ABORTED);
		}
		else
		{
			m_sql = m_database->lockHandle();

			if (mysql_query(m_sql, m_query.c_str()) != 0)
				triggerCallback(WorkerThread::QUERY_ERROR);
			else
				triggerCallback(WorkerThread::QUERY_SUCCESS);

			m_database->unlockHandle();
		}
	}

	poll();

	return 0;
}

void Query::triggerCallback(int status)
{
	setStatus(QUERY_READING_DATA);

	switch (status)
	{
		case WorkerThread::QUERY_ABORTED:
		{
			Result* result = new Result();
			result->setAborted(true);

			addResult(result);
			break;
		}
		default:
		{
			populateResults();
			break;
		}
	}

	m_shouldCallback = true;
}

void Query::populateResults()
{
	MYSQL* sql = m_worker ? m_worker->getHandle() : m_sql;

	int status = 0;
	while (status != -1)
	{
		MYSQL_RES* res = mysql_store_result(sql);
		unsigned int errNum = mysql_errno(sql);

		Result* result = new Result();
		{
			result->setResult(res);
			result->setErrorID(errNum);
			result->setError(mysql_error(sql));
			result->setRowsAffected((double)mysql_affected_rows(sql));
			result->setLastID((double)mysql_insert_id(sql));
		}

		addResult(result);
		status = mysql_next_result(sql);
	}
}

void Query::callback()
{
	m_shouldCallback = false;
	setStatus(QUERY_COMPLETE);

	if (m_prepcallback != NULL)
	{
		MLUA->ReferencePush(m_prepcallback);

		if (!m_prepared)
			pushObject();
		makeLuaTable();

		MLUA->PCall(m_prepared ? 1 : 2, 0, 0);

		MLUA->ReferenceFree(m_prepcallback);
	}
}

int Query::makeLuaTable()
{
	MLUA->CreateTable();

	int resID = 1;
	for (Results::iterator it = m_results.begin(); it != m_results.end(); ++it) {
		Result* res = *it;

		MLUA->PushNumber(resID++);
		MLUA->CreateTable();
		{
			bool status = res->getErrorID() == 0;
			MLUA->PushBool(status); MLUA->SetField(-2, "Success");

			if (!status)
			{
				if (res->getAborted())
				{
					MLUA->PushBool(res->getAborted()); MLUA->SetField(-2, "Aborted");
				}
				else
				{
					MLUA->PushString(res->getError().c_str()); MLUA->SetField(-2, "Error");
					MLUA->PushNumber(res->getErrorID()); MLUA->SetField(-2, "ErrorID");
				}
			}
			else
			{
				MLUA->PushNumber(res->getRowsAffected()); MLUA->SetField(-2, "Affected");
				MLUA->PushNumber(res->getLastID()); MLUA->SetField(-2, "LastID");

				MLUA->CreateTable();
				populateDataTable(res->getResult());
				MLUA->SetField(-2, "Data");
			}
		}

		MLUA->SetTable(-3);

		delete res;
	}

	m_results.empty();

	return 0;
}

void Query::populateDataTable(MYSQL_RES* res)
{
	// will be null if there wasn't a result set. only results in an empty Data table
	if (res == NULL)
		return;

	MYSQL_ROW row = mysql_fetch_row(res);
	MYSQL_FIELD* fields = mysql_fetch_fields(res);

	int rowID = 1;
	while (row)
	{
		unsigned int field_count = mysql_num_fields(res);
		unsigned long* lengths = mysql_fetch_lengths(res);

		MLUA->PushNumber(rowID++);
		MLUA->CreateTable();

		for (unsigned int i = 0; i < field_count; i++)
		{
			if (row[i] == NULL)
				continue;
			else if (IS_NUM(fields[i].type) && fields[i].type != MYSQL_TYPE_LONGLONG)
				MLUA->PushNumber(atof(row[i]));
			else
				MLUA->PushString(row[i], lengths[i]);

			MLUA->SetField(-2, fields[i].name);
		}

		MLUA->SetTable(-3);

		row = mysql_fetch_row(res);
	}
}
