#include "CLASS_Database.h"
#include "CLASS_ConnectThread.h"
#include "CLASS_Query.h"
#include "CLASS_Statement.h"
#include "CLASS_WorkerThread.h"
#include "CLASS_MutexLocker.h"

#include <string.h>

#ifdef LINUX
#include <stdlib.h>
#endif

std::string fixNullTerms(const char* toFix, int len)
{
	std::string fixed;

	for (int i = 0; i < len; i++)
	{
		char curchar = toFix[i];
		if (curchar == 0x00 && i != len-1) {
			fixed = fixed + "\\0";
			i++;
		}
		else
			fixed = fixed + curchar;
	}

	return fixed;
}

Database::Database(lua_State* state)
	: LuaObjectBaseTemplate<Database>(state)
{
	MutexLocker lock(m_sqlMutex);
	m_sql = mysql_init(0);

	m_connectThread = new ConnectThread(this);
	m_workerThread = new WorkerThread(this);
}

Database::~Database(void)
{
	m_connectThread->wait();
	delete m_connectThread;
	m_connectThread = 0;

	m_workerThread->wait();
	delete m_workerThread;
	m_workerThread = 0;

	MutexLocker lock(m_sqlMutex);
	mysql_close(m_sql);
	m_sql = 0;
}

BEGIN_BINDING(Database);
BIND_FUNCTION("Connect", Database::connect);
BIND_FUNCTION("Disconnect", Database::disconnect);
BIND_FUNCTION("Query", Database::query);
BIND_FUNCTION("Prepare", Database::prepare);
BIND_FUNCTION("Wait", Database::wait);
BIND_FUNCTION("Status", Database::status);
BIND_FUNCTION("Escape", Database::escape);
BIND_FUNCTION("GetCharSet", Database::getCharSet);
BIND_FUNCTION("SetCharSet", Database::setCharSet);
BIND_FUNCTION("HostInfo", Database::hostInfo);
BIND_FUNCTION("ServerInfo", Database::serverInfo);
BIND_FUNCTION("ServerVersion", Database::serverVersion);
BIND_FUNCTION("GetAllQueries", Database::getAllQueries);
END_BINDING();

void Database::setHost(const char* host)
{
	conData.host = host;
}

void Database::setUser(const char* user)
{
	conData.user = user;
}

void Database::setPass(const char* pass)
{
	conData.pass = pass;
}

void Database::setDatabase(const char* database)
{
	conData.database = database;
}

void Database::setPort(unsigned int port)
{
	conData.port = port;
}

void Database::setUnixSocket(const char* socket)
{
	conData.socket = socket;
}

void Database::setClientFlag(unsigned int flag)
{
	conData.flag = flag;
}

MYSQL* Database::lockHandle()
{
	m_sqlMutex.lock();
	return m_sql;
}

void Database::unlockHandle()
{
	m_sqlMutex.unLock();
}

void Database::poll()
{	if (!m_connectThread)
		return;

	Thread::EventData event;
	if (m_connectThread->getEvent(event))
	{
		if (event.id == ConnectThread::CONNECTION_FINISHED)
		{
			if (m_connectThread->wasSuccessful())
				runCallback("OnConnected");
			else
				runCallback("OnConnectionFailed", "s", m_connectThread->error().c_str());
		}
	}

	checkTaskStack();
}

void Database::clearFromStack(Query* query)
{
	for (std::vector<Query*>::iterator it = m_taskStack.begin();
	it != m_taskStack.end(); )
	{
		if ((*it) == query)
		{
			m_taskStack.erase(it);
			return;
		}

		it++;
	}
}

void Database::checkTaskStack()
{
	if (m_workerThread->getFinished())
	{
		for (std::vector<Query*>::iterator it = m_taskStack.begin();
		it != m_taskStack.end(); )
		{
			if ((*it)->getStatus() == QUERY_WAITING)
			{ // Assign our worker thread to the next query in line that's not running :D
				m_workerThread->setQuery((*it));
				(*it)->setWorker(m_workerThread);

				break;
			}

			it++;
		}
	}
}

void Database::waitAll()
{
	checkTaskStack();

	for (std::vector<Query*>::iterator it = m_taskStack.begin();
	it != m_taskStack.end(); )
	{
		(*it)->wait();
	}
}

int Database::connect()
{
	if (m_connectThread->isRunning())
		return 0;

	m_connectThread->start();
	
	return 0;
}

int Database::disconnect()
{
	if (!m_connectThread)
		return 0;
	if (!m_connectThread->wasSuccessful())
		return 0;

	runCallback("OnDisconnect");

	delete this;

	return 0;
}

int Database::query()
{
	if (!checkArgument(2, GarrysMod::Lua::Type::STRING))
		return 0;

	if (m_connectThread->isRunning())
		return 0;
	if (!m_connectThread->wasSuccessful())
		return 0;

	const char* query = MLUA->GetString(2);
	if (!query)
		return 0;

	Query* newQuery = new Query(this, m_luaState);
	newQuery->setQuery(query);
	newQuery->pushObject();

	return 1;
}

int Database::prepare()
{
	if (!checkArgument(2, GarrysMod::Lua::Type::STRING))
		return 0;

	if (m_connectThread->isRunning())
		return 0;
	if (!m_connectThread->wasSuccessful())
		return 0;

	const char* query = MLUA->GetString(2);
	if (!query)
		return 0;

	Statement* newStmt = new Statement(this, m_luaState);
	newStmt->setQuery(query);

	newStmt->pushObject();

	return 1;
}

int Database::wait()
{
	if (!m_connectThread)
		return 0;
	if (!m_connectThread->isRunning())
		return 0;

	m_connectThread->wait();
	poll();

	return 0;
}

int Database::status()
{
	if (!m_connectThread)
	{
		MLUA->PushNumber(DATABASE_INTERNAL_ERROR);
		return 1;
	}

	if (m_connectThread->isRunning())
	{
		MLUA->PushNumber(DATABASE_CONNECTING);
		return 1;
	}

	if (!m_connectThread->wasSuccessful())
	{
		MLUA->PushNumber(DATABASE_NOT_CONNECTED);
		return 1;
	}

	m_sqlMutex.lock();
	if (mysql_ping(m_sql) == 0)
	{
		MLUA->PushNumber(DATABASE_CONNECTED);
	}
	else
	{
		MLUA->PushNumber(DATABASE_NOT_CONNECTED);
	}
	m_sqlMutex.unLock();

	return 1;
}

int Database::escape()
{
	if (!checkArgument(2, GarrysMod::Lua::Type::STRING))
		return 0;

	unsigned int length;
	const char* sQuery = MLUA->GetString(2, &length);
	char* sEscapedQuery = Escape(sQuery, length);

	if (!sEscapedQuery)
		return 0;

	MLUA->PushString(sEscapedQuery);
	free(sEscapedQuery);

	return 1;
}

char* Database::Escape(const char* sQuery, unsigned int length)
{
	std::string str = fixNullTerms(sQuery, length);
	size_t nQueryLength = str.length();

	char* sEscapedQuery = (char*)malloc(nQueryLength * 2 + 1);

	if (sEscapedQuery == 0)
		return NULL;

	m_sqlMutex.lock();
	mysql_real_escape_string(m_sql, sEscapedQuery, sQuery, (unsigned long)nQueryLength);
	m_sqlMutex.unLock();

	return sEscapedQuery;
}

int Database::getCharSet()
{
	if (!m_connectThread->wasSuccessful())
		return 0;

	m_sqlMutex.lock();
	MY_CHARSET_INFO charset;
	mysql_get_character_set_info(m_sql, &charset);
	m_sqlMutex.unLock();
	
	MLUA->PushString(charset.csname);
	MLUA->PushString(charset.name);

	return 2;
}
int Database::setCharSet()
{
	if (!m_connectThread->wasSuccessful())
		return 0;

	if (!checkArgument(2, GarrysMod::Lua::Type::STRING))
		return 0;

	const char* charset = MLUA->GetString();
	if (!charset)
		return 0;

	m_sqlMutex.lock();
	mysql_set_character_set(m_sql, charset);
	m_sqlMutex.unLock();

	return 0;
}

int Database::hostInfo()
{
	if (!m_connectThread ||
		m_connectThread->isRunning() ||
		!m_connectThread->wasSuccessful())
	{
		MLUA->PushNil();
		return 1;
	}

	m_sqlMutex.lock();
	const char* info = mysql_get_host_info(m_sql);
	if (info)
		MLUA->PushString(info);
	else
		MLUA->PushNil();
	m_sqlMutex.unLock();

	return 1;
}

int Database::serverInfo()
{
	if (!m_connectThread ||
		m_connectThread->isRunning() ||
		!m_connectThread->wasSuccessful())
	{
		MLUA->PushNil();
		return 1;
	}

	m_sqlMutex.lock();
	const char* info = mysql_get_server_info(m_sql);
	if (info)
		MLUA->PushString(info);
	else
		MLUA->PushNil();
	m_sqlMutex.unLock();

	return 1;
}

int Database::serverVersion()
{
	if (!m_connectThread ||
		m_connectThread->isRunning() ||
		!m_connectThread->wasSuccessful())
	{
		MLUA->PushNil();
		return 1;
	}

	m_sqlMutex.lock();
	MLUA->PushNumber(mysql_get_server_version(m_sql));
	m_sqlMutex.unLock();

	return 1;
}

int Database::getAllQueries()
{
	MLUA->CreateTable();
	for (std::vector<Query*>::iterator it = m_taskStack.begin();
	it != m_taskStack.end(); )
	{
		MLUA->PushNumber(m_taskStack.end() - it);
		
		(*it)->pushObject();

		MLUA->SetTable(-3);

		it++;
	}

	return 1;
}