#include "CLASS_ConnectThread.h"
#include "CLASS_MutexLocker.h"
#include "CLASS_Database.h"

std::string safeStrToStr(const char* str)
{
	return (str ? std::string(str) : std::string());
}

const char* stringOrNull(const std::string& str)
{
	return (str.empty() ? 0 : str.c_str());
}

ConnectThread::ConnectThread(Database* dbase)
	: Thread(), m_mysql(dbase)
{
}

ConnectThread::~ConnectThread(void)
{
}

bool ConnectThread::init()
{
	mysql_thread_init();

	return true;
}

void ConnectThread::exit()
{
	mysql_thread_end();
}

void ConnectThread::done()
{
}

bool ConnectThread::wasSuccessful()
{
	return m_success;
}

std::string ConnectThread::error()
{
	return m_error;
}

int ConnectThread::run()
{
	MYSQL* sql = m_mysql->lockHandle();

	ConnectData cd = m_mysql->getConnectData();

	if (!mysql_real_connect(sql, stringOrNull(safeStrToStr(cd.host)), stringOrNull(safeStrToStr(cd.user)), stringOrNull(safeStrToStr(cd.pass)), stringOrNull(safeStrToStr(cd.database)), (int)cd.port, stringOrNull(safeStrToStr(cd.socket)), (int)cd.flag))
	{
		m_success = false;
		m_error = std::string(mysql_error(sql));

		postEvent(CONNECTION_FINISHED);

		m_mysql->unlockHandle();
		return 0;
	}
	else
	{
		m_success = true;
		m_error = "";
		
		postEvent(CONNECTION_FINISHED);

		m_mysql->unlockHandle();
		return 0;
	}
}