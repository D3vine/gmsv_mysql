#ifndef _CLASS_STATEMENT_H_
#define _CLASS_STATEMENT_H_

#include "CLASS_LuaObjectBase.h"
#include "MYSQL.h"

class Database;
class Query;
class WorkerThread;
class Result;

class Statement :
	public LuaObjectBaseTemplate<Statement, 218>
{
	LUA_OBJECT
public:
	Statement(Database* dbase, lua_State* state);
	virtual ~Statement();

	virtual void poll();

	void setQuery(const char* query) { m_query = std::string(query); }

	int run();
	int wait();
private:
	Database* m_dbase;

	std::string m_query;
	bool m_wait;
};

#endif