#ifndef _CLASS_QUERY_THREAD_H
#define _CLASS_QUERY_THREAD_H

#include "MYSQL.h"
#include "CLASS_Thread.h"
#include <string>

class Database;
class Query;
class Statement;

class WorkerThread :
	public Thread
{
public:
	enum
	{
		QUERY_ERROR = 0,
		QUERY_SUCCESS_NO_DATA,
		QUERY_DATA,
		QUERY_SUCCESS,
		QUERY_COLUMNS,
		QUERY_ABORTED,
	};
	enum
	{
		INTEGER = 0,
		FLOATING_POINT,
		STRING,
	};

	WorkerThread(Database* dbase);
	virtual ~WorkerThread(void);

	void reset();
	void setQuery(Query* query) { m_query = query; }
	Query* getQuery() { return m_query; }

	MYSQL* getHandle() { return m_sql; }

	void setFinished(bool fin) { m_finished = fin; }
	bool getFinished() { return m_finished; }

	virtual bool init();
	virtual int run();
	virtual void exit();
private:
	Database* m_dbase;
	Query* m_query = NULL;
	Statement* m_statement;
	MYSQL* m_sql = 0;

	bool m_finished = true;
};

#endif