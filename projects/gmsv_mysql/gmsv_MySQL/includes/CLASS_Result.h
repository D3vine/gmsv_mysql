#ifndef _CLASS_RESULT_H_
#define _CLASS_RESULT_H_

#include "MYSQL.h"
#include <string>

class Result
{
public:
	Result() { }
	~Result() { mysql_free_result(m_result); }

	void setErrorID(int err) { m_errorNum = err; }
	const int getErrorID() { return m_errorNum; }

	void setError(const char* err) { m_errorStr.assign(err); }
	const std::string& getError(void) { return m_errorStr; }

	void setLastID(double id) { m_lastID = id; }
	double getLastID() { return m_lastID; }

	void setRowsAffected(double rows) { m_rowsAffected = rows; }
	double getRowsAffected() { return m_rowsAffected; }

	void setResult(MYSQL_RES* res) { m_result = res; }
	MYSQL_RES* getResult() { return m_result; }

	void setAborted(bool aborted) { m_aborted = aborted; }
	bool getAborted() { return m_aborted; }

private:
	std::string m_errorStr;
	int m_errorNum;
	double m_lastID;
	double m_rowsAffected;
	bool m_aborted;
	MYSQL_RES* m_result;
};

#endif