#ifndef _CLASS_DATABASE_H_
#define _CLASS_DATABASE_H_

#include "MODULE_LuaOO.h"
#include "MYSQL.h"
#include "CLASS_Mutex.h"
#include "CLASS_Query.h"
#include "CLASS_Statement.h"
#include <vector>

std::string fixNullTerms(const char* toFix, int len);

class ConnectThread;
class WorkerThread;

enum {
	DATABASE_CONNECTED = 0,
	DATABASE_CONNECTING,
	DATABASE_NOT_CONNECTED,
	DATABASE_INTERNAL_ERROR,
};

struct ConnectData {
	const char* host = "localhost";
	const char* user = "user";
	const char* pass = "pass";
	const char* database = "dbase";
	unsigned int port = 3306;
	const char* socket = 0;
	unsigned int flag = CLIENT_MULTI_RESULTS | CLIENT_MULTI_QUERIES | CLIENT_INTERACTIVE;
};

class Database :
	public LuaObjectBaseTemplate<Database, 215>
{
	LUA_OBJECT
public:
	Database(lua_State* state);
	virtual ~Database(void);

	virtual void poll();
	void checkTaskStack();

	void setHost(const char* host);
	void setUser(const char* user);
	void setPass(const char* pass);
	void setDatabase(const char* database);
	void setPort(unsigned int port);
	void setUnixSocket(const char* socket);
	void setClientFlag(unsigned int flag);

	MYSQL* lockHandle();
	void unlockHandle();

	ConnectData getConnectData() { return conData; }

	void clearFromStack(Query* query);
	void addQueryToStack(Query* query) { m_taskStack.push_back(query); }

	void waitAll();

	int connect();
	int disconnect();
	int query();
	int prepare();
	int wait();
	int status();
	int escape();
	char* Escape(const char* sQuery, unsigned int length);
	int getCharSet();
	int setCharSet();
	int hostInfo();
	int serverInfo();
	int serverVersion();
	int getAllQueries();
private:
	ConnectData conData;

	Mutex m_sqlMutex;
	MYSQL* m_sql;

	ConnectThread* m_connectThread;
	WorkerThread* m_workerThread;

	std::vector<Query*> m_taskStack;
};

#endif