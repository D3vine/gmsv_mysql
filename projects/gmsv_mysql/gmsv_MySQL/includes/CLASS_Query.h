#ifndef _CLASS_QUERY_H_
#define _CLASS_QUERY_H_

#include "CLASS_LuaObjectBase.h"
#include "CLASS_WorkerThread.h"
#include "MYSQL.h"

class Database;
class WorkerThread;
class Result;

typedef std::vector<Result*> Results;

enum
{
	QUERY_NOT_RUNNING = 0,
	QUERY_RUNNING,
	QUERY_READING_DATA,
	QUERY_COMPLETE,
	QUERY_ABORTED,
	QUERY_WAITING,
	QUERY_DELETE_READY
};

class Query :
	public LuaObjectBaseTemplate<Query, 216>
{
	LUA_OBJECT
public:
	Query(Database* dbase, lua_State* state);
	virtual ~Query(void);

	virtual bool canDelete();

	void setQuery(const char* query) { m_query = std::string(query); }
	void setQuery(std::string query) { m_query = query; }
	std::string getQuery() { return m_query; }
	void setWorker(WorkerThread* worker) { m_worker = worker; if (m_worker) { m_worker->start(); m_status = QUERY_RUNNING; } }
	WorkerThread* getWorker() { return m_worker; }

	virtual void poll();

	bool running();
	bool finished() { return m_status == QUERY_DELETE_READY; };

	int getStatus() { return m_status; }
	void setStatus(int stat) { m_status = stat; }

	void setAborted(bool aborted) { m_aborted = aborted; }
	bool getAborted() { return m_aborted; }

	int start();
	int isRunning();
	int abort();
	int status();
	int wait();

	void callback();
	void triggerCallback(int status);

	void populateResults();
	int makeLuaTable();
	void populateDataTable(MYSQL_RES* res);

	void setPrepared(bool prep) { m_prepared = prep; }
	bool isPrepared() { return m_prepared; }
	void setCallback(int cb) { m_prepcallback = cb; }
	int getCallback() { return m_prepcallback; }

private:
	Database* m_database;
	WorkerThread* m_worker = NULL;
	MYSQL* m_sql;

	int m_status = QUERY_WAITING;
	std::string m_query;
	bool m_finished = false;
	bool m_deleteWorker = false;
	bool m_shouldCallback = false;
	bool m_aborted = false;
	bool m_prepared = false;
	bool m_waited = false;

	int m_prepcallback;
	// Members to hold any data sets the server returns
	Results m_results;
	int m_resultsRef;
	void addResult(Result* res) { m_results.push_back(res); }
};

#endif