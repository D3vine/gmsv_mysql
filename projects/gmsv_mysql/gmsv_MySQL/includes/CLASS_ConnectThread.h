#ifndef _CLASS_CONNECTTHREAD_H_
#define _CLASS_CONNECTTHREAD_H_

#include "CLASS_Thread.h"
#include "CLASS_Mutex.h"
#include <string>
#include "MYSQL.h"

class Database;

class ConnectThread : public Thread
{
public:
	enum { CONNECTION_FINISHED = 0 };

	ConnectThread(Database* dbase);
	virtual ~ConnectThread(void);

	bool wasSuccessful();
	std::string error();

	virtual bool init();
	virtual int run();
	virtual void exit();
	virtual void done();
private:
	Database* m_mysql;

	std::string m_error;
	bool m_success;
};

#endif